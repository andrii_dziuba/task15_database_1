use labor_sql;
#task 1.1
select maker, type from product order by maker; 

#task 1.2
select model, ram, screen, price from laptop where price<=1000 order by ram, price desc;

#task 1.3
select * from printer where color='y';

#task 1.4
select model, speed, hd, cd, price from pc where cd='12x' or cd='24x' and price<600 order by speed desc; 

#task 1.5
select distinct name, class from ships order by name;

#task 1.6
select * from pc where speed>=500 and price<=800 order by price desc; 

#task 1.7
select * from printer where type<>'Matrix' and price<300 order by type desc;

#task 1.8
select model, speed from pc where price>=400 and price<=600 order by hd asc;

#task 1.9
select pc.model, speed, hd from pc inner join product p on pc.model=p.model where hd=20 or hd=10 and p.maker='A';

#task 1.10
select model, speed, hd, price from laptop where screen>=12 order by price desc;

#task 1.11
select model, type, price from printer where price<300 order by type desc;

#task 1.12
select model, ram, price from laptop where ram=64 order by screen;

#task 1.13
select model, ram, price from pc where ram>64 order by hd;

#task 1.14
select model, speed, price from pc where speed>=500 and speed<=750 order by hd;

#task 1.15
select * from outcome_o where outcome_o.out>2000 order by outcome_o.date desc;

#task 1.16
select * from income_o where inc>=5000 and inc<=10000 order by inc;

#task 1.17
select * from income order by inc;

#task 1.18
select * from outcome order by outcome.out;

#task 1.19
select * from ships inner join classes c on ships.class=c.class where country='Japan' order by type desc;

#task 1.20
select name, launched from ships where launched>=1920 and launched<=1942 order by launched desc;

#task 1.21
select ship, battle, result from outcomes where battle='Guadalcanal' and result<>'sunk' order by ship desc;

#task 1.22
select ship, battle, result from outcomes where result='sunk' order by ship desc;

#task 1.23
select class, displacement from classes where displacement>=40000 order by type;

#task 1.24
select trip_no, town_from, town_to from trip where town_from='London' or town_to='London' order by time_out;

#task 1.25
select trip_no, plane, town_from, town_to from trip where plane='TU-134' order by time_out desc;

#task 1.26
select trip_no, plane, town_from, town_to from trip where plane='IL-86' order by plane desc;

#task 1.27
select trip_no, town_from, town_to from trip where town_from<>'Rostov' and town_to<>'Rostov' order by plane;

#task 2.1
select * from pc where model rlike '1{2,}';

#task 2.2
select * from outcome where outcome.date rlike '-03-';

#task 2.3
select * from outcome_o where outcome_o.date rlike '-14 ';

#task 2.4
select name from ships where ships.name rlike '^W.*n$';

#task 2.5
select name from ships where ships.name rlike '[e].*[e]';

#task 2.6
select name, launched from ships where ships.name not rlike 'a$';

#task 2.7
select name from battles where name rlike '.+ .+[^c]$';

#task 2.8
select * from trip where hour(time_out)>=12 and hour(time_out)<=17;

#task 2.9
select * from trip where hour(time_in)>=17 and hour(time_in)<=23;

#task 2.10
select trip_no, date, place from pass_in_trip where place like '1%';

#task 2.11
select trip_no, date, place from pass_in_trip where place like '%c';

#task 2.12
select name from passenger where name like '% C%';

#task 2.13
select name from passenger where name rlike '.+ [^J][^ ]+$';

#task 3.1
select maker, type, speed, hd from pc p inner join product pr on p.model=pr.model where hd<=8;

#task 3.2
select maker from pc p inner join product pr on p.model=pr.model where speed>=600;

#task 3.3
select maker from laptop p inner join product pr on p.model=pr.model where speed<=500;

#task 3.4
select * from laptop p1
	left join laptop p2 on p1.ram=p2.ram and p1.hd=p2.hd
	where p1.code<>p2.code
	order by p1.code desc, p1.hd desc, p1.ram desc;
        
#task 3.5
select distinct country, class from classes where type='bb' or type='bc';

#task 3.6
select p.model, maker from pc p inner join product pr on p.model=pr.model where price<600;

#task 3.7
select p.model, maker from printer p inner join product pr on p.model=pr.model where price>300;

#task 3.8
select maker, p.model, price from laptop p inner join product pr on p.model=pr.model;

#task 3.9
select maker, p.model, price from pc p inner join product pr on p.model=pr.model;

#task 3.10
select maker, type, l.model, speed from laptop l inner join product p on l.model=p.model where speed>600;

#task 3.11
select name, displacement from ships s inner join classes c on s.class=c.class;

#task 3.12 
select distinct battle, date from outcomes o inner join battles b on o.battle=b.name where result='OK' and o.ship not in (select s.ship from (select distinct ship from outcomes where result<>'OK') as s); 

#task 3.13
select name, country from ships s left join classes c on s.class=c.class;

#task 3.14
select trip_no, c.name from trip t inner join company c on t.ID_comp=c.ID_comp where plane='Boeing'; 

#task 3.15
select name, t.date from passenger p inner join pass_in_trip t on p.ID_psg=t.ID_psg;