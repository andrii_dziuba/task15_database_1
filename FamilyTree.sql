-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema family_tree_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema family_tree_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `family_tree_db` DEFAULT CHARACTER SET utf8 ;
USE `family_tree_db` ;

-- -----------------------------------------------------
-- Table `family_tree_db`.`gender`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `family_tree_db`.`gender` (
  `gender` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`gender`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `family_tree_db`.`family_companion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `family_tree_db`.`family_companion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `date_of_birth` DATE NULL,
  `full_info` VARCHAR(500) GENERATED ALWAYS AS (CONCAT(first_name, ' ', last_name, ' birth ', date_of_birth, ' anno Domino ')) VIRTUAL,
  `date_of_death` DATE NULL,
  `place_of_birth` VARCHAR(45) NULL,
  `place_of_death` VARCHAR(45) NULL,
  `date_of_marriage` VARCHAR(45) NULL,
  `gender_gender` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_family_companion_gender1_idx` (`gender_gender` ASC) VISIBLE,
  CONSTRAINT `fk_family_companion_gender1`
    FOREIGN KEY (`gender_gender`)
    REFERENCES `family_tree_db`.`gender` (`gender`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `family_tree_db`.`family_tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `family_tree_db`.`family_tree` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `full_name` VARCHAR(90) GENERATED ALWAYS AS (CONCAT(first_name, last_name)) VIRTUAL,
  `date_of_birth` DATE NULL,
  `date_of_death` DATE NULL,
  `place_of_birth` VARCHAR(45) NULL,
  `place_of_death` VARCHAR(45) NULL,
  `card_number` VARCHAR(45) NULL,
  `family_tree_id` INT NOT NULL,
  `gender_gender` VARCHAR(15) NOT NULL,
  `family_companion_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_family_tree_family_tree_idx` (`family_tree_id` ASC) VISIBLE,
  INDEX `fk_family_tree_gender1_idx` (`gender_gender` ASC) VISIBLE,
  INDEX `fk_family_tree_family_companion1_idx` (`family_companion_id` ASC) VISIBLE,
  CONSTRAINT `fk_family_tree_family_tree`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `family_tree_db`.`family_tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_gender1`
    FOREIGN KEY (`gender_gender`)
    REFERENCES `family_tree_db`.`gender` (`gender`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_family_companion1`
    FOREIGN KEY (`family_companion_id`)
    REFERENCES `family_tree_db`.`family_companion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `family_tree_db`.`family_jewels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `family_tree_db`.`family_jewels` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `approximate_price` DOUBLE NULL,
  `max_price` DOUBLE NULL,
  `min_price` DOUBLE NULL,
  `catalog_code` DECIMAL NULL,
  `coefficient` DOUBLE GENERATED ALWAYS AS (SIN(min_price)+COS(max_price)) VIRTUAL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `family_tree_db`.`family_tree_has_family_jewels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `family_tree_db`.`family_tree_has_family_jewels` (
  `family_tree_id` INT NOT NULL,
  `family_jewels_id` INT NOT NULL,
  INDEX `fk_family_tree_has_family_jewels_family_tree1_idx` (`family_tree_id` ASC) VISIBLE,
  INDEX `fk_family_tree_has_family_jewels_family_jewels1_idx` (`family_jewels_id` ASC) VISIBLE,
  PRIMARY KEY (`family_tree_id`, `family_jewels_id`),
  CONSTRAINT `fk_family_tree_has_family_jewels_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `family_tree_db`.`family_tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_has_family_jewels_family_jewels1`
    FOREIGN KEY (`family_jewels_id`)
    REFERENCES `family_tree_db`.`family_jewels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

INSERT INTO `family_tree_db`.`gender` (`gender`) VALUES ('female');
INSERT INTO `family_tree_db`.`gender` (`gender`) VALUES ('male');


INSERT INTO `family_tree_db`.`family_companion` (`first_name`, `last_name`, `date_of_birth`, `date_of_death`, `place_of_birth`, `place_of_death`, `date_of_marriage`, `gender_gender`) VALUES ('Імя1', 'Прізвище1', '1996-01-01', '1996-01-01', 'місце народження1', 'місце смерті1', '1996-01-01', 'male');
INSERT INTO `family_tree_db`.`family_companion` (`first_name`, `last_name`, `date_of_birth`, `date_of_death`, `place_of_birth`, `place_of_death`, `date_of_marriage`, `gender_gender`) VALUES ('Імя2', 'Прізвище2', '2141-01-01', '7755-01-01', 'місце народження2', 'місце смерті4', '5346-01-01', 'female');
INSERT INTO `family_tree_db`.`family_companion` (`first_name`, `last_name`, `date_of_birth`, `date_of_death`, `place_of_birth`, `place_of_death`, `date_of_marriage`, `gender_gender`) VALUES ('Імя3', 'Прізвище3', '5534-01-01', '7678-01-01', 'місце народження1', 'місце смерті1', '8568-01-01', 'male');


INSERT INTO `family_tree_db`.`family_tree` (`first_name`, `last_name`, `date_of_birth`, `date_of_death`, `place_of_birth`, `place_of_death`, `card_number`, `gender_gender`) VALUES ('Чувак1', 'тіпа прізвище', '2143-01-01', '2145-01-01', 'планета Земля', 'планета Земля', '21412412', 'male');
INSERT INTO `family_tree_db`.`family_tree` (`first_name`, `last_name`, `date_of_birth`, `date_of_death`, `place_of_birth`, `place_of_death`, `card_number`, `gender_gender`) VALUES ('Чувак2', 'тіпа прізвище', '2143-01-01', '2145-01-01', 'планета Земля', 'планета Земля', '21412412', 'male');
INSERT INTO `family_tree_db`.`family_tree` (`first_name`, `last_name`, `date_of_birth`, `date_of_death`, `place_of_birth`, `place_of_death`, `card_number`, `gender_gender`) VALUES ('Чувак3', 'тіпа прізвище', '2143-01-01', '2145-01-01', 'планета Земля', 'планета Земля', '21412412', 'male');

